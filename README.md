# Profiling

This project is supposed to be included as a submodule to facilitate profiling support of FastAPI based services.

## Dependencies

Before you can use this project as a submodule, be sure to add `pyinstrument` to your project's dependencies, e.g. using poetry

```shell
poetry add pyinstrument
```

## Usage

```python
# suppose the submodule is located at ./profiling
from .profiling import add_profiling

app = FastAPI()

if settings.enable_profiling:
    add_profiling(app)
```

## Exploration

Navigate to your service's `/profiling` route which will list HTML and JSON files for each request containing detailed profiling information.
