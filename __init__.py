import datetime
import os
import pathlib
import tempfile

from fastapi.responses import HTMLResponse
from fastapi.staticfiles import StaticFiles
from pyinstrument import Profiler
from pyinstrument.renderers import HTMLRenderer, JSONRenderer


def add_profiling(app):
    directory = pathlib.Path(tempfile.mkdtemp())

    root_path = app.root_path or ""
    stripped_root_path = root_path.strip("/")

    app.mount(f"{stripped_root_path}/profiling-static", StaticFiles(directory=directory), name="profiling")

    @app.middleware("http")
    async def _(request, call_next):
        if request.url.path.startswith(f"{root_path}/profiling"):
            return await call_next(request)

        profiler = Profiler(async_mode="enabled")

        profiler.start()
        try:
            response = await call_next(request)
        finally:
            profiler.stop()
            basename = datetime.datetime.now().isoformat(timespec="microseconds").replace(":", "-")
            basename += request.url.path.replace("/", "_")
            htmlpath = directory / pathlib.Path(basename + ".html")
            htmlpath.write_text(profiler.output(HTMLRenderer(show_all=True)))
            jsonpath = directory / pathlib.Path(basename + ".json")
            jsonpath.write_text(profiler.output(JSONRenderer(show_all=True)))

        return response

    @app.get(f"{stripped_root_path}/profiling", status_code=200, tags=["Private"], response_class=HTMLResponse)
    async def _():
        paths = sorted(directory.glob("*.html"))
        items = []
        for path in paths:
            item_title = f'{path.name[path.name.find("_"):-5].replace("_", "/")}'
            html_href = f'<a href="{stripped_root_path}/profiling-static/{path.name}">HTML</a>'
            json_href = f'<a href="{stripped_root_path}/profiling-static/{path.name[:-5]}.json">JSON</a>'
            items.append(f"<li>{item_title}: [{html_href}] [{json_href}]</li>")
        listing = "\n".join(items)
        html = f"""
        <html>
        <body>
        <h2>Profiling Overview</h2>
        <ul>
          {listing}
        </ul>
        </body>
        </html>
        """
        return HTMLResponse(content=html, status_code=200)
